<?php

/*
Plugin Name: Jupiter Check Tracking Data
Plugin URI: https://artbees.net/
Description: Used to check Tracking Data in Database.
Version: 0.0.2
Author: ayubadiputra
Author URI: https://github.com/ayubadiputra
License: GPLv2 or later
Text Domain: jct_data
*/

if ( ! class_exists( 'Jupiter_Check_Tracking' ) ) {

    /**
     * JCT main class.
     *
     * Create page and admin menu for displaying Tracking Data.
     *
     * @class   Jupiter_Check_Tracking
     * @package Jupiter_Check_Tracking
     * @since   0.0.1
     * @version 0.0.2
     */
    class Jupiter_Check_Tracking {

        private $jct_options;
        private $jct_c_panel;

        /**
         * Start up
         */
        public function __construct() {
            add_action( 'admin_menu', array( $this, 'jct_tracking_page' ) );
            add_action( 'after_setup_theme', array( $this, 'jct_run_after' ) );
        }

        /**
         * Run actions after all loaded.
         */
        public function jct_run_after() {
            if ( has_action( 'jupiter_control_panel_tracking' ) ) {
                if ( ! empty( $_GET['jct-get-status'] ) ) {
                    if ( $_GET['jct-get-status'] === 'inactive' ) {
                        // Silence is golden.
                    } elseif ( $_GET['jct-get-status'] === 'reset' ) {
                        delete_option( 'jupiter-control-panel-tracking' );
                    }
                } else {
                    do_action( 'jupiter_control_panel_tracking' );
                }
            }
        }

        /**
         * Add options page
         */
        public function jct_tracking_page() {
            // This page will be under "Settings"
            add_options_page(
                'Jupter Tracking',
                'Jupiter Check Tracking Data',
                'manage_options',
                'jct-data',
                array( $this, 'jct_tracking_view' )
            );
        }

        /**
         * Options page callback
         * @version 0.0.2
         */
        public function jct_tracking_view() {
            // Set class property
            $this->jct_options = get_option( 'jupiter-data-tracking-result' );
            $this->jct_c_panel = get_option( 'jupiter-control-panel-tracking' );
            ?>
            <div class="wrap">
                <h2>Jupiter Tracking Data</h2>
                <h2>Control Panel</h2>
                <form>
                    <p>
                        <label>
                            You can check all Jupiter Data tracked in this site (Control Panel) in the raw format below:
                        </label>
                    </p>
                    <p>
                    <?php
                        echo '<pre>';
                        var_dump( $this->jct_c_panel );
                        echo '</pre>';
                    ?>
                    </p>
                </form>
                <h2>Daily</h2>
                <form>
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th scope="row">Blog Styles Used</th>
                                <td id="front-static-pages">
                                    <fieldset>
                                        <?php
                                        $blog_styles = ( ! empty( $this->jct_options['blog_styles'] ) ) ? $this->jct_options['blog_styles'] : null;
                                        if ( ! empty( $blog_styles ) ) :
                                            foreach ( $blog_styles as $key => $value ) :
                                        ?>
                                        <p>
                                            <label><?php echo ucfirst( $key ); ?> : <?php echo $value; ?></label>
                                        </p>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Portfolio Styles Used</th>
                                <td id="front-static-pages">
                                    <fieldset>
                                        <?php
                                        $portfolio_styles = ( ! empty( $this->jct_options['portfolio_styles'] ) ) ? $this->jct_options['portfolio_styles'] : null;
                                        if ( ! empty( $portfolio_styles ) ) :
                                            foreach ( $portfolio_styles as $key => $value ) :
                                        ?>
                                        <p>
                                            <label><?php echo ucfirst( $key ); ?> : <?php echo $value; ?></label>
                                        </p>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </fieldset>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p>
                        <label>
                            You can check all Jupiter Data tracked in this site (blog_styles, portfolio_styles) in the raw format below:
                        </label>
                    </p>
                    <p>
                    <?php
                        echo '<pre>';
                        var_dump( $this->jct_options );
                        echo '</pre>';
                    ?>
                    </p>
                </form>
            </div>
            <?php
        }
    }
}

if ( is_admin() )
    $jct = new Jupiter_Check_Tracking();